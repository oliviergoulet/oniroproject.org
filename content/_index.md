---
title: "Oniro"
date: 2021-10-06T10:00:00-04:00
footer_class: "footer-darker"
header_wrapper_class: "featured-jumbotron-home"
headline: The Open Source Operating System Platform for Connecting Consumer Devices Big and Small 
jumbotron_class: "jumbotron-circle"
hide_breadcrumb: true
hide_page_title: true
container: "container-fluid"
hide_sidebar: true
show_featured_footer: false
seo_title: "Oniro"
---

{{< home/about-us >}}

{{< home/working-group >}}

{{< home/join-us >}}

{{< home/supporters >}}

{{< home/testimonials >}}
