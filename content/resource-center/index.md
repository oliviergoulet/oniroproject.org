---
title: "Resource Center"
date: 2021-10-06T10:00:00-04:00
footer_class: "footer-darker"
hide_sidebar: true
layout: "single"
---

The process to create the Oniro Working Group and Top Level Project has been an
intensive  collaborative effort by the founding members of the Working Group. A
number of the resulting resources can be accessed here, currently hosted at
Eclipse:

- [Get the Code](https://gitlab.eclipse.org/eclipse/oniro-core)
- [Documentation](http://docs.oniroproject.org/)
- [Talk to us](https://docs.oniroproject.org/en/latest/community-chat-platform.html) at [Eclipse Foundation Chat Service](https://chat.eclipse.org/#/room/#oniro:matrix.eclipse.org) - space #oniro:matrix.eclipse.org

Learn more about the Oniro project at the Eclipse Foundation:

- [Oniro Top Level Project](https://projects.eclipse.org/projects/oniro)
- [Top Level Charter](https://projects.eclipse.org/projects/oniro/charter)

