---
title: "Join Us"
seo_title: "Join Oniro Working Group"
keywords: ["join Oniro", "Oniro", "open source", "working group"]
---

* [Public developers mailing list](https://accounts.eclipse.org/mailing-list/oniro-dev)
* [Public working group's mailing list](https://accounts.eclipse.org/mailing-list/oniro-wg)
* [Community's chat](https://docs.oniroproject.org/en/latest/community-chat-platform.html)
* [Follow Oniro on Twitter](https://twitter.com/@oniro_project)
* [Detailed procedure to join](https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/marketing-oniro/-/wikis/Join_Oniro)
* [Contact Oniro's staff](https://oniroproject.org/join-us/)

{{< hubspot_contact_form portalId="5413615" formId="1279b5cf-d70f-4977-b585-b6885fac1d75" >}}
